<?php

namespace BlogBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ArticleRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ArticleRepository extends EntityRepository {

    public function getPaginated($pageNo, $limit, $categoryId = null) {
        $firstResult = ($pageNo - 1) * $limit;
        $queryBuilder = $this->createQueryBuilder('alias')
            ->where('alias.deleted = 0');
        if ($categoryId) {
            $queryBuilder->andWhere('alias.category = :categoryId')
                ->setParameter('categoryId', $categoryId);
        }
        $queryBuilder = $queryBuilder->setFirstResult($firstResult)
            ->setMaxResults($limit);
        return $queryBuilder->getQuery()->getResult();
    }

    public function countArticles($categoryId = null) {
        $queryBuilder = $this->createQueryBuilder('alias')
            ->where('alias.deleted = 0');
        if ($categoryId) {
            $queryBuilder->andWhere('alias.category = :categoryId')
                ->setParameter('categoryId', $categoryId);
        }
        $queryBuilder = $queryBuilder->select('COUNT(alias)');
        return $queryBuilder->getQuery()->getSingleScalarResult();
    }
}
