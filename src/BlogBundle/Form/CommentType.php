<?php

namespace BlogBundle\Form;

use BlogBundle\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class CommentType extends AbstractType {

    const MAX_COMMENT_LENGTH = 255;

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('content', TextareaType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'max' => self::MAX_COMMENT_LENGTH,
                        'maxMessage' => sprintf(
                            'Article content cannot be longer than %s characters',
                            self::MAX_COMMENT_LENGTH
                        )
                    ]),
                ],
            ])
            ->add('parentId', HiddenType::class, [
                'mapped' => FALSE,
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['value' => 'Submit']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
        ]);
    }
}