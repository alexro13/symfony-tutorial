<?php

namespace BlogBundle\Form;

use BlogBundle\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType {

    const MAX_NAME_LENGTH = 50;
    const MAX_MAIL_LENGTH = 100;
    const MAX_CONTENT_LENGTH = 500;

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('firstName',null, [
                'label' => 'First name',
                'attr' => [
                    'maxlength' => self::MAX_NAME_LENGTH,
                ],
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'max' => self::MAX_NAME_LENGTH,
                        'maxMessage' => sprintf(
                            'First name cannot be longer than %s characters',
                            self::MAX_NAME_LENGTH
                        )
                    ]),
                ],
            ])
            ->add('lastName',null, [
                'label' => 'Last name',
                'attr' => [
                    'maxlength' => self::MAX_NAME_LENGTH,
                ],
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'max' => self::MAX_NAME_LENGTH,
                        'maxMessage' => sprintf(
                            'Last name cannot be longer than %s characters',
                            self::MAX_NAME_LENGTH
                        )
                    ]),
                ],
            ])
            ->add('email',null, [
                'label' => 'Email',
                'attr' => [
                    'maxlength' => self::MAX_MAIL_LENGTH,
                ],
                'constraints' => [
                    new Email(),
                    new NotBlank(),
                    new Length([
                        'max' => self::MAX_MAIL_LENGTH,
                        'maxMessage' => sprintf(
                            'The email address cannot be longer than %s characters',
                            self::MAX_MAIL_LENGTH
                        )
                    ]),
                ],
            ])
            ->add('content', null, [
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'max' => self::MAX_CONTENT_LENGTH,
                        'maxMessage' => sprintf(
                            'Article content cannot be longer than %s characters',
                            self::MAX_CONTENT_LENGTH
                        )
                    ]),
                ],
            ])
            ->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}