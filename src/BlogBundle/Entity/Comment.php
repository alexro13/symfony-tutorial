<?php

namespace BlogBundle\Entity;

/**
 * Comment
 */
class Comment
{
    use TimeStampLoggerTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $content;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Comment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Comment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Comment
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @var User
     */
    protected $author_id;

    public function getAuthor() {
        return $this->author_id;
    }

    /**
     * @param User $author
     * @return $this
     */
    public function setAuthor($author) {
        $this->author_id = $author;

        return $this;
    }

    /**
     * @var Article
     */
    protected $article_id;

    public function getArticle() {
        return $this->article_id;
    }

    /**
     * @param  $article
     * @return $this
     */
    public function setArticle($article) {
        $this->article_id = $article;

        return $this;
    }

    /**
     * @var Comment
     */
    protected $parent_id;

    public function getParent() {
        return $this->parent_id;
    }

    /**
     * @param  $parent
     * @return $this
     */
    public function setParent($parent) {
        $this->parent_id = $parent;

        return $this;
    }
    /**
     * @var Comment
     */
    protected $children;

    public function getChildren() {
        return $this->children;
    }

    /**
     * @param  Comment[] $children
     * @return $this
     */
    public function setChildren($children) {
        $this->children = $children;

        return $this;
    }
}
