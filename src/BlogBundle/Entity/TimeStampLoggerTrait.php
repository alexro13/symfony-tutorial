<?php

namespace BlogBundle\Entity;

trait TimeStampLoggerTrait {

  /**
   * @var \DateTime
   */
  protected $createdAt;

  /**
   * @var \DateTime
   */
  protected $updatedAt;

  /**
   * Sets the created at value.
   */
  public function setCreatedAtValue() {
    $this->createdAt = new \DateTime();
  }

  /**
   * Sets the updated at value.
   */
  public function setUpdatedAtValue() {
    $this->updatedAt = new \DateTime();
  }
}