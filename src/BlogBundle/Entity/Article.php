<?php

namespace BlogBundle\Entity;

/**
 * Article
 *
 * @BlogBundle\Validator\Constraints\NotContainsArticleTitlesConstraint
 */
class Article {

  use TimeStampLoggerTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @BlogBundle\Validator\Constraints\NotContainsPercentageConstraint
     * @BlogBundle\Validator\Constraints\NotContainsUnethicalContentConstraint
     *
     * @var string
     */
    private $title;

    /**
     * @BlogBundle\Validator\Constraints\NotContainsUnethicalContentConstraint
     *
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $image;

    /**
     * @var bool
     */
    private $deleted = FALSE;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Article
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Article
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Article
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Article
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * @var \BlogBundle\Entity\ArticleCategory
     */
    private $category;

    /**
     * Set category
     *
     * @param \BlogBundle\Entity\ArticleCategory $category
     *
     * @return Article
     */
    public function setCategory(ArticleCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \BlogBundle\Entity\ArticleCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return bool
     */
    public function getDeleted() {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }

    protected $comments;

    public function setComments($comments) {
        $this->comments = $comments;

        return $this;
    }

    public function getComments() {
        return $this->comments;
    }
}
