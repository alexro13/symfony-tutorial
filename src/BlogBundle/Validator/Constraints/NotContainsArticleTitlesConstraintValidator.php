<?php

namespace BlogBundle\Validator\Constraints;

use BlogBundle\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class NotContainsArticleTitlesConstraintValidator extends ConstraintValidator {

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Article $article
     * @param Constraint $constraint
     */
    public function validate($article, Constraint $constraint)
    {
        if ($this->containsTitle($article->getContent(), $article->getTitle())) {
            $this->createViolation($constraint, $article->getTitle());

            return;
        }

        $articles = $this->em->getRepository(Article::class)->findAll();
        foreach ($articles as $dbArticle) {
            if ($this->containsTitle($article->getContent(), $dbArticle->getTitle())) {
                $this->createViolation($constraint, $dbArticle->getTitle());
                break;
            }
        }
    }

    protected function containsTitle($content, $title) {
        return strstr($content, $title) !== false;
    }

    protected function createViolation($constraint, $title) {
        return $this->context->buildViolation($constraint->message)
            ->setParameter('%title%', $title)
            ->addViolation();
    }
}
