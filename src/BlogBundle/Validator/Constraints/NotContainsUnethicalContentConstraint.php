<?php

namespace BlogBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotContainsUnethicalContentConstraint extends Constraint {
    const NOT_ALLOWED_WORD = 'beep';
    public $message = 'The string %string% contains the word ' . self::NOT_ALLOWED_WORD . ', which is not allowed!';
}