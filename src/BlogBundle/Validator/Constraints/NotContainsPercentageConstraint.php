<?php

namespace BlogBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotContainsPercentageConstraint extends Constraint {
    const NOT_ALLOWED_CHARACTER = '%';
    public $message = 'The string %string% contains the character ' . self::NOT_ALLOWED_CHARACTER . ', which is not allowed!';
}