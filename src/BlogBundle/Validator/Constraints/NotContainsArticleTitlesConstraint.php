<?php

namespace BlogBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotContainsArticleTitlesConstraint extends Constraint {
    public $message = 'The content contains %title% which already exists as a title of an article.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
    public function validatedBy()
    {
        return 'not_contains_article_titles';
    }
}
