<?php

namespace BlogBundle\Controller;

use BlogBundle\Entity\Article;
use BlogBundle\Entity\ArticleCategory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CategoryController extends Controller {
    public function __categoryMenuAction() {
        $repo = $this->getDoctrine()->getManager()->getRepository(ArticleCategory::class);
        $categories = $repo->findAll();

        return $this->render('BlogBundle:Category:categoryMenu.html.twig', [
          'categories' => $categories
        ]);
    }

    public function listByCategoryAction(ArticleCategory $category) {
        $repo = $this->getDoctrine()->getManager()->getRepository(Article::class);
        $request = $this->get('request_stack')->getCurrentRequest();

        $pageNo = $request->query->getInt('page', 1);
        $limit = $request->query->getInt('limit', 5);
        $pageCount = ceil($repo->countArticles($category) / $limit);
        $articles = $repo->getPaginated($pageNo, $limit, $category->getId());

        return $this->render('BlogBundle:Article:articleList.html.twig', [
            'articles' => $articles,
            'currentLimit' => $limit,
            'limits' => ArticleController::PAGINATION_LIMITS,
            'currentPage' => $pageNo,
            'totalPages' => $pageCount,
            'paginationRouteName' => 'category_list',
            'paginationRouteParams' => ['id' => $category->getId()],
        ]);
    }
}
