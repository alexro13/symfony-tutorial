<?php

namespace BlogBundle\Controller;

use BlogBundle\Entity\Contact;
use BlogBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends Controller {

    public function createAction(Request $request, Contact $contact = null) {
        $contact = $contact ?: new Contact();

        $form = $this->createForm(ContactType::class, $contact);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if (empty($contact->getId())) {
                $em->persist($contact);
            }
            $em->flush();

            $message = \Swift_Message::newInstance()
                ->setSubject('Contact Email')
                ->setFrom($contact->getEmail())
                ->setTo('alex.stanciu.dev@gmail.com')
                ->setBody(
                    $this->renderView(
                        'BlogBundle:Emails:contact.html.twig',
                        array('contact' => $contact)
                    ),
                    'text/html'
                );

            $this->get('mailer')->send($message);

            return $this->redirectToRoute('blog_homepage');
        }

        return $this->render('@Blog/Contact/contactCreate.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function deleteAction(Contact $contact) {
        $em = $this->getDoctrine()->getManager();

        $em->remove($contact);
        $em->flush();

        return $this->redirectToRoute('blog_homepage');
    }

}
