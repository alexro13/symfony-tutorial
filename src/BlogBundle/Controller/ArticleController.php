<?php

namespace BlogBundle\Controller;

use BlogBundle\Entity\Article;
use BlogBundle\Entity\Comment;
use BlogBundle\Entity\User;
use BlogBundle\Form\ArticleType;
use BlogBundle\Form\CommentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ArticleController extends Controller {

    const PAGINATION_LIMITS = [2,5,10,15,20];

    public function indexAction() {
        $request = $this->get('request_stack')->getCurrentRequest();
        $repo = $this->getDoctrine()->getManager()->getRepository(Article::class);

        $pageNo = $request->query->getInt('page', 1);
        $limit = $request->query->getInt('limit', 5);
        $pageCount = ceil($repo->countArticles() / $limit);

        // If the page and limit are not correct, redirect with default parameters.
        if (!in_array($limit, self::PAGINATION_LIMITS) || $pageNo < 1 || $pageNo > $pageCount) {
            return $this->redirectToRoute('blog_homepage', [
                'limit' => 5,
                'page' => 1
            ]);
        }

        $articles = $repo->getPaginated($pageNo, $limit);
        $pageCount = ceil($repo->countArticles() / $limit);

        return $this->render('BlogBundle:Article:articleList.html.twig', [
            'articles' => $articles,
            'currentLimit' => $limit,
            'limits' => self::PAGINATION_LIMITS,
            'currentPage' => $pageNo,
            'totalPages' => $pageCount,
            'paginationRouteName' => 'blog_homepage',
            'paginationRouteParams' => [],
        ]);
    }

    public function detailAction(Request $request, User $user, Article $article) {

        $comment = new Comment();

        $comment_form = $this->createForm(CommentType::class, $comment);

        $comment_form->handleRequest($request);

        if ($comment_form->isSubmitted() && $comment_form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $parentId = $comment_form->get('parentId')->getData();
            $parent = null;
            if (is_numeric($parentId)) {
                $parent = $em->getRepository(Comment::class)->find($parentId);
                if (!($parent instanceof Comment) || $parent->getArticle()->getId() != $article->getId()) {
                    throw new BadRequestHttpException("Something went wrong. Please try again or contact a system administrator");
                }
            }
            $comment->setArticle($article);
            $comment->setAuthor($user);
            $comment->setContent($comment_form->get('content')->getData());
            if ($parent != null) {
                $comment->setParent($parent);
            }
            if (empty($comment->getId())) {
                $this->addFlash(
                    'notice',
                    'Your comment was posted!'
                );
                $em->persist($comment);
            }
            $em->flush();

            return $this->redirect($request->getUri());
        }

        return $this->render('BlogBundle:Article:articleDetail.html.twig', [
            'article' => $article,
            'comment_form' => $comment_form->createView(),
        ]);
    }

    public function createAction(Request $request, Article $article = null) {
        $article = $article ?: new Article();

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if (empty($article->getId())) {
                $em->persist($article);
            }
            $em->flush();

            return $this->redirectToRoute('blog_homepage');
        }

        return $this->render('@Blog/Article/articleCreate.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function deleteAction(Article $article) {
        $em = $this->getDoctrine()->getManager();

        $article->setDeleted(TRUE);
        $em->persist($article);
        $em->flush();

        return $this->redirectToRoute('blog_homepage');
    }

}
