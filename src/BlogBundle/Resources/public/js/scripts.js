var BLOG = BLOG || {};

BLOG.registerDeleteConfirmation = function() {
  var deleteBtn = $('.delete-btn');
  if (deleteBtn.length === 0) {
    return;
  }
  deleteBtn.on('click', function(event){
    var isConfirmed = confirm('Are you sure you want to delete this?');
    if (!isConfirmed) {
      event.preventDefault();
    }
  });
};

BLOG.handleCommentSubmit = function() {
  var $replyButtons = $('.reply-btn');
  if ($replyButtons.length === 0) {
    return;
  }

  $replyButtons.on('click', function(event) {
    // trigger modal
    $('#myModal').modal({});
    // get the pressed button element from the event
    var $btn = $(event.currentTarget);
    // find comment main block, which hold's the id of the comment the user wants to reply
    var $commentRoot = $btn.parents('.comment-details');
    // if there is no such a parent, we're trying to reply directly to the article
    var commentIdToReply = $commentRoot.length > 0 ? $commentRoot.data('id') : null;

    // update the parent Id hidden input's value
    $('#comment_parentId').val(commentIdToReply);
  });
};

$(document).ready(function(){
  BLOG.registerDeleteConfirmation();
  BLOG.handleCommentSubmit();
});
